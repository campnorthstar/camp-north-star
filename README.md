We are the top Maine sleepaway camp for children. Our overnight camp in Maine teaches life skills to our New England campers at our sleep away camp.



Contact Us!

Contact Person: Steven Bernstein

Tel: 207-998-4777 

Email: info@campnorthstarmaine.com

Location: 200 Verrill Road, Poland Spring, ME 04274, USA

Website: https://www.campnorthstarmaine.com/


Elsewhere online

https://www.facebook.com/CNSMaine

https://twitter.com/campnorthstar

https://plus.google.com/102136729780229676030

https://www.youtube.com/channel/UCbVHocRR1VBhmvEYTarpkLg

https://www.linkedin.com/company/camp-north-star-maine/